# Docker Pets na Microsoft Azure

**Documentação que irá auxiliar subir uma aplicação na Microsoft Azure integrando os processos de CI/CD.**

Requisitos:
- Conta na Microsoft Azure

## Criando novo recurso do Azure Container Registry

Primeiro passo vamos criar uma conta free na [Microsoft Azure](https://azure.microsoft.com/pt-br/). Depois de criado e logado no Microsoft Azure, o próximo passo será criar um ***Resource Group***, que tem por finalidade agrupar os recursos do Azure, permitindo o monitoramento, controle de acesso e de custos.

![Resource Group Button](./images/imagem1.png)
![Resource Group Form](./images/imagem2.png)

Após preencher os dados clicar em review e create.

Para essa aplicação usaremos o registry da própria Azure para. Clique em **Create a recource** e procure por **Container Registry**.

![Container Registry Button](./images/imagem3.png)
![Container Registry Form](./images/imagem4.png)

Após criado o registry vamos habilitar o user admin para o mesmo.

![Enable Admin](./images/imagem16.png)


## Criando novo projeto no Azure DevOps

O Projeto proposto pela **Agility** foi clonado e commitado em um novo repositório para fins de testes de CI/CD, alocado agora no Bitbucket, e uma mudança foi feita no arquivo app.py, alterando a porta que sobe a aplicação para 80.

**Novo repositório onde o fonte está versionado**
https://brumalmeida@bitbucket.org/brumalmeida/docker-pets.git

Com nosso registry já criado, precisamos agora criar uma pipeline para gerar e subir essa imagem. 

Vá até a [Azure Devops](https://dev.azure.com/), crie uma conta grátis para darmos inicio a criação da pipeline.

***Obs:** Se for o primeiro acesso ao Azure Devops, certifique-se de criar uma organização.*

A primeira coisa que vamos fazer no **Azure Devops** será integra-lo com o registry que criamos no **Microsoft Azure**, para fazer essa integração basta clicar em project settings > service connections > new service connection, na tela selecione docker registry.

![Docker Registry Button](./images/imagem8.png)

Em **New Docker Registry service connection**

* Selecione a opção **Azure Container Registry** em Registry Type;
* Escolhe sua assinatura do **Azure** a ser utilizada em Subscription;
* Em **Azure container registry**, selecione o registry que foi criado nos passos anteriores;
* Em **Service connection name** dar um nome a sua conexão;
* Em **Security** marcar a opção **Grant access permission to all pipelines**.

Logo após clique em save.

![Docker Registry Form](./images/imagem9.png)

## Pipeline para build automatizado das Imagens Docker

Vamos agora criar nosso projeto.

![New Project Form](./images/imagem5.png)

Escolha o nome do seu projeto e logo após em create. Será solicitado o local onde está versionado o código fonte da aplicação, no nosso caso no **Bitbucket**.

![Code Versions](./images/imagem14.png)

Na etapa seguinte selecione a opção **Starter pipeline**, posicione seu cursor na última linha do arquivo, clique no botão **Show assistant** e procure por **Docker**. Preencha essas informações conforme o registry criado nas etapas anteriores.

Após preencher os valores, clique em **Add** e um novo bloco de código será criado no final do arquivo.

***Obs:** A linha trigger é o branch que vamos utilizar para disparar nosso pipeline quando algo for commitado na mesma.*

![Show Assistant Docker Form](./images/imagem6.png)

O Azure Devops irá criar um novo arquivo chamado ***azure-pipelines.yml***, será preciso agora commitar essa mudança no fonte e logo após o build será executado.

![Job Logs](./images/imagem12.png)

Até agora já temos nosso registry criado e nossa imagem montada e armazenada no mesmo. Caso algum commit for efetuado na branch **master**, o pipeline será disparado, um novo build da imagem será criado e subida para o registry.

## Criando recuro no Azure para Web App for Container

Chegamos na etapa de colocar essa nossa imagem para rodar em cloud, e para isso vamos utilizar o **Web App for Containers**, que pode ser acessado no **Portal do Azure**.

![Web app for Containers Button](./images/imagem17.png)

Certifique-se de que a opção **Docker Container** esteja marcada e preencha os campos com valores dos recursos criados anteriormente. Após isso clique em **Next: Docker >**.

![Create Wep App Form](./images/imagem18.png)

Na seção **Docker**:

* Em **Options** deverá estar marcada a opção **Single Container**;
* Em **Image Source** deverá estar marcada a opção **Azure Container Registry**;
* Em **Registry**, selecione o registry que foi criado nos passos anteriores;
* Em **Image** e **Tag**, selecionar as mesmas que foram geradas anteriormente.

Clicar em seguida em **Review + create**.

![Create Wep App Docker Form](./images/imagem19.png)

Será apresentada uma tela para revisar as informações, assim que conferido clique em **Create**.

![Create Web App Review](./images/imagem20.png)

Podemos observar agora na próxima imagem o nosso recurso já criado e o endereço de acesso.

![Wep app Overview](./images/imagem21.png)
![Application Site](./images/imagem22.png)

## Crtiando um pipeline de release/deployment automatizado

Agora finalmente vamos para o último passo que será um release/deployment automatizado da nossa aplicação na **Azure**, usando a imagem criada em nosso registry e o **Azure Web App for Containers**.

Acesse o **Azure DevOps** e vá até **Pipelines**, clique na opção **Releases** e em seguida no botão **New Pipeline**.

![New Pipeline Button](./images/imagem24.png)

Em **Select Template** selecione a opção **Azure App Service deployment** e clique em **Apply**.

![Azure App Servie deployment Button](./images/imagem27.png)

Preencha o campo **Stage Name** com o valor **Deploy App**, e em seguida clique na opção **1 job, 1 task**.

![1 job 1 task Button](./images/imagem29.png)

Nas opções abertas, configure da seguinte maneira:

* Em **Azure Description**, selecione sua assinatura do **Azure**;
* Em **App Type**, selecione a opção **Web App for Containers (Linux)**;
* Em **App service name**, selecione o recurso do **Azure Web App for Containers** criado nos passos anteriores;
* Em **Registry or Namespace**, selecione o registry que criamos logo no início do artigo + o endereço do registry da **Azure** (Exemplo: seuregistry.azurcr.io);
* Em **Repository** preencha com o nome da imagem que foi gerada nos passos anteriores.

Após esses passos clique no botão **Save**, onde logo após um comentário podera ser adicionado.

![Release Pipeline Form](./images/imagem39.png)

Clique agora na opção **+ Add** em **Artifacts**

![Add Artifact Button](./images/imagem31.png)

Em **Add an artifact**:

* Deixe selecionado a opção **Build**;
* Em **Source (build pipeline)**, selecione o pepeline de build de imagens montado anteriormente;
* Em **Default Version**, vamos deixar com a tag **latest**, para sempre fazer o deploy com a última versão;
* Em **Source alias** deixe selecionado o repositório que foi empregado no build das imagens Docker.

![Add Artifact Form](./images/imagem32.png)

Nosso pŕoximo passo agora será ativar o deploy automatizado da aplicação. Para isso vamos acionar a opção **Continuos deployment trigger** em **Artifacts** e marcar o item **Continuos deployment trigger** como **Enabled**. Logo após clique em **Save** e em **OK**.

![Continuos deployment trigger Button](./images/imagem33.png)

## Teste de deploy Automatizado

Agora que já temos toda nossa pipeline pronta, vamos fazer um teste, iremos realizar um commit na branch **master** e verificar se o pipelone será disparado no **Azure DevOps**.

Observe que o commit foi gerado no **Bitbucket**, e o pipeline foi disparado no **Azure DevOps**.

![Commit Teste Deploy](./images/imagem35.png)

![Trigger Pipeline build image](./images/imagem36.png)

Após finalizado o build da imagem o **Azure DevOps** irá disparar a pipeline de **release**

![Trigger Release Pipeline](./images/imagem37.png)
![Trigger Release Pipeline status](./images/imagem40.png)

Finalizado sua aplicação já estará novamente disponível para ser acessada e atualizada!

![App Site](./images/imagem41.png)